#! /bin/bash

#
# NOTE: Add support for upgrade
#
echo -n "Updating..."
git fetch 2>/dev/null
if git status --short --branch | grep -q behind; then
    git pull >/dev/null 2>&1
    echo " Restarting..."
    exec "$0" "$@"
fi
echo " Done."

echo "File 6"
